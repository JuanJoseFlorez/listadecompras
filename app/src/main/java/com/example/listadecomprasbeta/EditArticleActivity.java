package com.example.listadecomprasbeta;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.listadecomprasbeta.models.Articles;

import io.realm.Realm;

public class EditArticleActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etDescription;
    private EditText etQuantity;
    private EditText etCost;
    private Button btnSave;

    private Realm realm;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_article);
        realm = Realm.getDefaultInstance();
        loadViews();
        loadToolbar();
        id = getIntent().getStringExtra("articleId");
        loadArticle(id);
    }

    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.edit_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadArticle(String id) {
        Articles articles = realm.where(Articles.class).equalTo("id", id).findFirst();
        if (articles != null) {
            etName.setText(articles.getName());
            etDescription.setText(articles.getDescription());
            etQuantity.setText(articles.getQuantity());
            etCost.setText(articles.getCost());
        }
    }

    private void loadViews() {
        etName = findViewById(R.id.et_name);
        etDescription = findViewById(R.id.et_description);
        etQuantity = findViewById(R.id.et_quantity);
        etCost = findViewById(R.id.et_cost);
        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateArticle();
            }
        });
    }

    private void updateArticle() {
        String name = etName.getText().toString();
        String description = etDescription.getText().toString();
        String quantity = etQuantity.getText().toString();
        String cost = etCost.getText().toString();
        Articles articles = new Articles(id, name, description, quantity, cost);
        updateArticleInDB(articles);
        finish();
    }

    private void updateArticleInDB(Articles articles) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(articles);
        realm.commitTransaction();
    }
}
