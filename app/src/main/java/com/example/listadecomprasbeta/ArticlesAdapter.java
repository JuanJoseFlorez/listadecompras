package com.example.listadecomprasbeta;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.listadecomprasbeta.models.Articles;

import java.util.List;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ItemViewHolder> {

    private Context context;
    private List<Articles> items;
    private ArticlesAdapterListener listener;

    public ArticlesAdapter(Context context, List<Articles> items, ArticlesAdapterListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ArticlesAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View template = inflater.inflate(R.layout.articles_template, parent, false);
        return new ArticlesAdapter.ItemViewHolder(template);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticlesAdapter.ItemViewHolder holder, int position) {
        final Articles articles = items.get(position);
        holder.tvName.setText(articles.getName());
        holder.tvDescription.setText(articles.getDescription());
        holder.tvQuantity.setText(articles.getQuantity());
        holder.tvCost.setText(articles.getCost());
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deleteArticle(articles.getId());
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.editArticle(articles.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateArticles(List<Articles> articles) {
        this.items = articles;
        notifyDataSetChanged();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvDescription;
        private TextView tvQuantity;
        private TextView tvCost;
        private ImageView ivEdit;
        private ImageView ivDelete;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvQuantity = itemView.findViewById(R.id.tv_quantity);
            tvCost = itemView.findViewById(R.id.tv_cost);
            ivEdit = itemView.findViewById(R.id.iv_edit);
            ivDelete = itemView.findViewById(R.id.iv_delete);
        }
    }

    public interface ArticlesAdapterListener {

        void deleteArticle(String id);

        void editArticle(String id);
    }
}
