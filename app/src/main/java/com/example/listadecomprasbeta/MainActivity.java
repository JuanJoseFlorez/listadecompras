package com.example.listadecomprasbeta;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.listadecomprasbeta.models.Articles;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements ArticlesAdapter.ArticlesAdapterListener, SearchView.OnQueryTextListener {


    private RecyclerView recyclerView;
    private Realm realm;
    private ArticlesAdapter articlesAdapter;
    private FloatingActionButton btnAdd;
    private TextView tvCost;
    private String id;
    private String cost;
    private RecyclerView myRecyclerView;
    private TextView listaVacia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        realm = Realm.getDefaultInstance();
        recyclerView = findViewById(R.id.recycler_view);
        loadArticles();
        loadViews();
        loadToolbar();
        listaVacia();


    }

    private void listaVacia() {
        if(myRecyclerView.getAdapter() != null){
            //De esta manera sabes si tu RecyclerView está vacío
            if(myRecyclerView.getAdapter().getItemCount() == 0) {
                String sms = "No hay datos para mostrar! :(";

                listaVacia.setText(sms);
                listaVacia.setVisibility(View.VISIBLE);
                myRecyclerView.setVisibility(View.GONE);
            }

        }
    }


    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.toolbar_title);
        }

    }

    private void loadViews() {
        listaVacia = findViewById(R.id.lista_vacia);
        myRecyclerView=findViewById(R.id.recycler_view);
        btnAdd = findViewById(R.id.fab);
        tvCost = findViewById(R.id.tv_cost);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addArticles();
            }
        });
    }

    private void addArticles() {
        Intent intent = new Intent(this, AddArticlesActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        updateArticles();
    }

    private void loadArticles() {
        List<Articles> articles = getArticles();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        articlesAdapter = new ArticlesAdapter(this, articles, this);
        recyclerView.setAdapter(articlesAdapter);
    }

    private List<Articles> getArticles() {
        List<Articles> articles = realm.where(Articles.class).findAll();
        return articles;
    }

    @Override
    public void deleteArticle(String id) {
        realm.beginTransaction();
        Articles articles = realm.where(Articles.class).equalTo("id", id).findFirst();
        if (articles != null) {
            articles.deleteFromRealm();
        }
        realm.commitTransaction();

        updateArticles();
        loadViews();
        listaVacia();

    }

    @Override
    public void editArticle(String id) {
        Intent intent = new Intent(this, EditArticleActivity.class);
        intent.putExtra("articleId", id);
        startActivity(intent);
    }

    private void updateArticles() {
        articlesAdapter.updateArticles(getArticles());
    }

    public boolean onCreateOptionMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}

