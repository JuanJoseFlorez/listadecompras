package com.example.listadecomprasbeta;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.listadecomprasbeta.models.Articles;

import java.util.UUID;

public class AddArticlesActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etDescription;
    private EditText etQuantity;
    private EditText etCost;
    private Button btnSave;
    private TextView tvCost;
    private int suma=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_articles);
        loadToolbar();
        loadViews();
        setupOnClicks();
    }

    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(R.string.add_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadViews() {
        etName = findViewById(R.id.et_name);
        etDescription = findViewById(R.id.et_description);
        etQuantity = findViewById(R.id.et_quantity);
        etCost = findViewById(R.id.et_cost);
        btnSave = findViewById(R.id.btn_save);
    }

    public void save(View view) {
        String name = etName.getText().toString().trim();
        String description = etDescription.getText().toString().trim();
        String quantity = etQuantity.getText().toString().trim();
        String cost = etCost.getText().toString().trim();

        boolean isSuccess = true;

        if (name.isEmpty()) {
            String message = getString(R.string.required);
            etName.setError(message);
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            isSuccess = false;
        }
        if (description.isEmpty()) {
            etDescription.setError(getString(R.string.required));
            isSuccess = false;
        }
        if (quantity.isEmpty()) {
            etQuantity.setError(getString(R.string.required));
            isSuccess = false;
        }
        if (cost.isEmpty()) {
            etCost.setError(getString(R.string.required));
            isSuccess = false;
        }

        if (isSuccess) {
            String id = UUID.randomUUID().toString();
            Articles articles = new Articles(id, name, description, quantity, cost);
            openConfirmActivity(articles);
        }

    }


    private void openConfirmActivity(Articles articles) {
        Intent intent = new Intent(this, ConfirmActivity.class);
        intent.putExtra("articles", articles);
//        startActivity(intent);
        startActivityForResult(intent, 100);
    }


    private void setupOnClicks() {
        View.OnClickListener onClick;
        onClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(v);
            }
        };
        btnSave.setOnClickListener(onClick);
    }
}
