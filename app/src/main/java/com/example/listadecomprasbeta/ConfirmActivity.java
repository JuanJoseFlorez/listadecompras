package com.example.listadecomprasbeta;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.listadecomprasbeta.models.Articles;

import io.realm.Realm;

public class ConfirmActivity extends AppCompatActivity {

    private Articles articles;
    private TextView tvName;
    private TextView tvDescription;
    private TextView tvQuantity;
    private TextView tvCost;
    public int suma=0;
    public int cost;
    public TextView tvCosts;

    private Button btnConfirm;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        realm = Realm.getDefaultInstance();
        loadViews();
        loadToolbar();

        articles = (Articles) getIntent().getSerializableExtra("articles");
        loadArticle();
    }
    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.confirm_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadArticle() {
        tvName.setText(articles.getName());
        tvDescription.setText(articles.getDescription());
        tvQuantity.setText(articles.getQuantity());
        tvCost.setText(articles.getCost());
    }

    private void loadViews() {
        tvCosts=findViewById(R.id.tv_costs);
        tvName = findViewById(R.id.tv_name);
        tvDescription = findViewById(R.id.tv_description);
        tvQuantity = findViewById(R.id.tv_quantity);
        tvCost = findViewById(R.id.tv_cost);
        btnConfirm = findViewById(R.id.btn_confirm);


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmArticle();
                seeArticles();
            }
        });
    }

    private void confirmArticle() {
        Toast.makeText(this, "Articulo confirmado", Toast.LENGTH_SHORT).show();
        addArticleInDB(articles);
    }



    private void addArticleInDB(Articles articles) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(articles);
        realm.commitTransaction();

    }


    private void seeArticles() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
